package ticket;

import java.rmi.Remote;
import java.rmi.RemoteException;
 
public interface TicketSeller extends Remote {
    public int ticket() throws RemoteException;
}
