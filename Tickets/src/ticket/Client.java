package ticket;

import java.rmi.Naming;
import java.rmi.RemoteException;

public class Client {
	public static void main(String[] args) throws Exception {
		int i;
		int N = 1000;
		Person t[] = new Person[N];
		RetVal n[] = new RetVal[N];
		int check[] = new int[N]; 
		String server=args[0];
		if ( args.length < 1 ) { System.err.println("usage: ..."); return; } 
		String url="rmi://"+server+":1111/MyServer";
		TicketSeller name = (TicketSeller) Naming.lookup(url);
		for (i=0; i<N; i++) {
			check[i]=0;
			n[i] = new RetVal();
			t[i] = new Person(name,n[i]);
			t[i].start();
		}
		int max=0;
		for (i=0; i<N; i++) {
			t[i].join();
			max=n[i].get()>max?n[i].get():max; 
		}
		for (i=0;i<N; i++) {
			check[max-n[i].get()]++;
		}
		boolean ok=true;
		for (i=0;i<N; i++) {
			if (check[i] != 1 ) {
				ok=false;
				System.out.format("Ticket %d sold %d times!%n",max-i,check[i]);
			}
		}
		if ( ok ) System.out.format("Ok!%n");
	}
	static class Person extends Thread {
		TicketSeller s;
		RetVal r;
		public Person(TicketSeller s, RetVal r) {
			this.r=r;
			this.s=s;
		}
		public void run() {
			try {
				r.set(s.ticket());
			} catch (RemoteException e) {
				e.printStackTrace();
				return;
			}
		}
	}

	static class RetVal {
		private int val=0;
		public void set(int i){val=i;};
		public int get(){return val;}
	}
}
