package ticket;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements TicketSeller {

	private static final long serialVersionUID = 1L;
	Counter x = new Counter();
	public Server() throws RemoteException {}
    public int ticket() throws RemoteException {
        return (x.increment());
        }
 
    public static void main(String[] args) throws RemoteException {
        System.setProperty("java.rmi.server.hostname", "192.168.5.1");
        Server objServer = new Server();
        Registry reg = LocateRegistry.createRegistry(1111);
        reg.rebind("MyServer", objServer);
        System.out.println("Server Ready");
    }
    static class Counter {
    	int i=0;
    	public synchronized int increment() { i=i+1; return i; }
    }

}
